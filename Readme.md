# TP3 : Cryptographie

## Binome

Le Guennec Yaakoub, yaakoub.leguennec.etu@univ-lille.fr 
Gatari Jean Debout, jeandebout.gatari.etu@univ-lille.fr


## Question 1
La solution qu'on propose c'est que chacun des deux responsables dispose d'une clé USB qui elle-même contient une clè cryptographie protegée par un mot de passe.

Une fois on a récuperé les clès cryptographiées des deux clès USB, on associe les deux clès ensemble en utilisant XOR pour avoir la clé principale.
Si on n'a pas accès à une de ces deux clès on ne pourra pas décrypter la clé principale qui sera stockée dans le RAMDISK.

La récupération de la clé principale nous permet de décrypter les paires nom/numéro des cartes bancaires qui sont dans disque.

Pour la partie d'authentification : On décrypte les deux clès cryptographiues à partir des clès USB et après on décypte les paires nom/numéro des cartes bancaires. Pour réussir à faire cela on décrypte avec la première clé puis avec la deuxième clé pour avoir le fichier crypté. 

## Question 2

### i. Mise en service de la partie Responsable et cryptage de la database

La création des dossiers et des fichiers nécessaires pour le traitement:
- RAMDISK
- disque 
- usb1
- usb2
- disque/database (contiendra les paires nom/numéro carte bancaire)

```
bash init.sh

```
Après exécution du script les fichiers cryptés seront créés avec des mots de passes pour plus de sécurité (les fichiers vont avoir un double mot de passe, un lors du cryptage et l'autre désigné par la commande gpg)


### ii. ajouter une paire

```
bash ajoutPaire.sh $nom $numero

```

### iii. supprimer une paire

```
bash suppPaire.sh $nom $numero

```
### iv. chercher les numero de cartes associer à un nom

```
bash search.sh $nom

```

Après cryptage de la base de données on supprime les fichiers temporaires

### décryptage de la database
 
```
bash demarrer.sh

```


## Question 3 :

La solution qu'on propose c'est qu'on garde la même idée de la question 1. C'est-à-dire, chaque représentant legal va avoir aussi une clé USB contenant une clé cryptographie. 
Pour différencier un responsable et un représentant légal, on peut ajouter un caractère ou quelques caractères dans la clé cryptographie pour différencier entre les clès des responsables et les clès des représentants

## Question 4 :

### i. Mise en service de la partie Representant et cryptage de la database

La création des dossiers des représenants:
- usb1Rep
- usb2Rep

```
bash initRep.sh

```
Après exécution du script les fichiers cryptés seront créés avec des mots de passes pour plus de sécurité (comme avec les responsables)

Les fichiers clès de chaque représentant vont contenir, avant le cryptage, un mot identique pour les différencier des fichiers clès des responsables


Cryptage de la base de données avec les clés des représentants

### décryptage de la database
 
```
bash demarrerRep.sh 

```
Après exécution du script de decryptage on demande quel utilisateur est en train d'exécuter avec un une question sur le terminal, le choix de l'utilisateur ouvre l'accès à une des 4 possiblités de decryptage, si l'utilisateur est le Responsable 1 par exemple, donc le script va décrypter la clè qui est stockée dans le dossier usb1 du Responsable 1. Si l'utilisateur est le Représentant 1 alors on décrypte la clè du usb1Rep, Cette fois-ci il faut pas oublier d'enlver le mot clé (Rep dans notre cas) de la clé donc on regarde si le mot existe et on l'enlève.

Après décryptage de la première clé on commence le décryptage de la deuxième, même démarche.

## Question 5 :

Pour cette solution on va utiliser la même méthode que pour la question précédente, on demande à l'utilisateur e choisir qui va être répudier. Si l'utilisateur choisit de répudier le Responsable 1 par exemple, il faut donc décrytper les autres clès ( pour les clès des representants il ne faut pas oublier d'enlever le mot clé), ensuite décrypter la base de données, puis créer les nouvelles clès et enfin crypter la base de données avec ces dernières. 

À chaque fois qu'un Responsable par exemple sera répudié on crée une nouvelle clé cryptée et protégée par un mot de passe pour le nouveau Responsable qui prend sa place. 

### Répudiation
 
```
bash repudiation.sh 

```
