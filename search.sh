#!/bin/bash
if [ $# -eq 1 ]
then
    FILE=disque/database
	if [ -f "$FILE" ]; #check if file exists
	then
		name=$1
		if grep "$name" "$FILE" #verify if the name exist in FILE (grep command used for the search)
		then
			echo "The name ("$1") found successfully"
		else
			echo "Name not found"
		fi
	else
		echo  "Error searching for name ! Interrupted"
	fi
else
	echo "Error ! Please provide 2 arguments"
fi

