#!/bin/bash

#Creating the necessary files and directories
rm -rf usb1Rep usb2Rep #delete directories if already exist to start the init 
mkdir usb1Rep  usb2Rep

#Creating key files
dd if=/dev/random bs=64 count=1 | hexdump -ve '1/1 "%02x"' > RAMDISK/key1Rep #hexdump -ve is used to diplay all input data in a string format
dd if=/dev/random bs=64 count=1 | hexdump -ve '1/1 "%02x"' > RAMDISK/key2Rep


#Creating encrypted files
echo "Decrytping Responsable 1 key"
openssl enc -d -pbkdf2 -aes-256-cbc -in usb1/key1.crypt -out RAMDISK/key1Rep
echo 'Rep' >> RAMDISK/key1Rep #Ajout d'un élément pour différencier entre un responsable et un représentant

echo "Decrytping Responsable 2 key"
openssl enc -d -pbkdf2 -aes-256-cbc -in usb2/key2.crypt -out RAMDISK/key2Rep
echo 'Rep' >> RAMDISK/key2Rep

echo "encrytping Represtant 1 key"
openssl enc -pbkdf2 -aes-256-cbc -in RAMDISK/key1Rep -out usb1Rep/key1.crypt


echo "encrytping Represtant 2 key"
openssl enc -pbkdf2 -aes-256-cbc -in RAMDISK/key2Rep -out usb2Rep/key2.crypt


#Crypting Database
openssl enc -pbkdf2 -aes-256-cbc -in disque/database -out disque/databasetmp.crypt -kfile RAMDISK/key1Rep
openssl enc -pbkdf2 -aes-256-cbc -in disque/databasetmp.crypt  -out disque/database.crypt -kfile RAMDISK/key2Rep

#Deleting temporary files
rm RAMDISK/key1Rep RAMDISK/key2Rep disque/database disque/databasetmp.crypt
