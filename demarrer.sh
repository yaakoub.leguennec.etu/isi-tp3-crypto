#!/bin/bash

FILE=disque/database

if [ -f "$FILE" ]; #Check file 
then
	echo "Commissioning"
else
    #Decrypting the first key
    echo "Decrypting Responsable 1 Key"

	if openssl enc -d -pbkdf2 -aes-256-cbc -in usb1/key1.crypt -out RAMDISK/key1
	then
        #When successfull then decrypt second key
		echo "Decrypting Responsable 2 Key"


		if openssl enc -d -pbkdf2 -aes-256-cbc -in usb2/key2.crypt -out RAMDISK/key2
		then
            # After successfully decrypting both then decrypt the database
			openssl enc -d -pbkdf2 -aes-256-cbc -in disque/database.crypt -out disque/databasetmp.crypt -kfile RAMDISK/key2
			openssl enc -d -pbkdf2 -aes-256-cbc -in disque/databasetmp.crypt -out disque/database -kfile RAMDISK/key1

            #Delete crypted files from disque		
			rm disque/databasetmp.crypt disque/database.crypt
					
		else 
			echo "Error decrypting Responsable 2 Key"
		fi

	else 
		echo "Error decrytping Responsable 1 Key"
	fi
fi


