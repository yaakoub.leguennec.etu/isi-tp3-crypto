#!/bin/bash
if [ $# -eq 2 ]
then
    FILE=disque/database
	if [ -f "$FILE" ]; #check if file exists 
	then
		pair=$1":"$2 
		if grep "$pair" "$FILE" #verify if the pairs exist in FILE (grep command used for the search)
        then
			sed "/$pair/d" disque/database > disque/tmpfile #insert the pair in a temporary file
			rm disque/database #delete the file containing the pair
			mv disque/tmpfile disque/database #rename the new file containing the new data with the main file (from tmpfile to database)

			echo "Pair ("$1","$2") deleted successfully"
		else
			echo "Pair doesn't exist"
		fi

	else
		echo  "Error deleteing pairs ! Interrupted"
	fi
else
	echo "Error ! Please provide 2 arguments"
fi

