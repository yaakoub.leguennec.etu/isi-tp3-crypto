#!/bin/bash


FILE=disque/database 

echo "Répudiation"
read -p "Who's going to be repudiated RES1,RES2,REP1,REP2 ? : " choice

if [ $choice = "RES1" ] #if we choose to repudiate Responsable 1 
then
	#Then first we have decrypt all the other keys (Rep1 && Rep2 && Res2)
	gpg "usb1Rep/key1.crypt.gpg"
	gpg "usb2Rep/key2.crypt.gpg"
	gpg "usb2/key2.crypt.gpg"

	if openssl enc -d -pbkdf2 -aes-256-cbc -in usb1Rep/key1.crypt -out RAMDISK/key1 && openssl enc -d -pbkdf2 -aes-256-cbc -in usb2Rep/key2.crypt -out RAMDISK/key2  && openssl enc -d -pbkdf2 -aes-256-cbc -in usb2/key2.crypt -out RAMDISK/key2	
	then 
		#we need to remove the Rep word from the keys
		sed "/Rep/d" RAMDISK/key1 > RAMDISK/tempK1
		rm RAMDISK/key1
		mv RAMDISK/tempK1 RAMDISK/key1

		#Decrypting database
		openssl enc -d -pbkdf2 -aes-256-cbc -in disque/database.crypt -out disque/databasetmp.crypt -kfile RAMDISK/key2
		openssl enc -d -pbkdf2 -aes-256-cbc -in disque/databasetmp.crypt  -out disque/database -kfile RAMDISK/key1
		
		rm disque/database.crypt disque/databasetmp.crypt

		# Creating the new Key for Res1 and Rep1
		dd if=/dev/random bs=64 count=1 > RAMDISK/key1
		cp RAMDISK/key1  RAMDISK/key1Rep #copying the Resp Key to create the Rep key
		echo 'Rep' >> RAMDISK/key1Rep #adding the key word to the Rep Key 

		echo " Key for the new Responsable 1"
		openssl enc -pbkdf2 -aes-256-cbc -in RAMDISK/key1 -out usb1/key1.crypt
		gpg -c usb1/key1.crypt

		echo " key for the new Representant 1"
		openssl enc -pbkdf2 -aes-256-cbc -in RAMDISK/key1Rep -out usb1Rep/key1.crypt
		gpg -c usb1Rep/key1.crypt

		#Crypting database with the new keys
		openssl enc -pbkdf2 -aes-256-cbc -in disque/database -out disque/databasetmp.crypt -kfile RAMDISK/key1
		openssl enc -pbkdf2 -aes-256-cbc  -in disque/databasetmp.crypt -out disque/database.crypt -kfile RAMDISK/key2

		#Deleting temporary files
		rm RAMDISK/key1 RAMDISK/key2 RAMDISK/key1Rep disque/database disque/databasetmp.crypt
	else 
		echo "Invalid Key ! Please try again ! "	
	fi


elif [ $choice = "RES2" ] #if we choose to repudiate Responsable 2
then
	#Then first we have decrypt all the other keys (Rep1 && Rep2 && Res1)
	gpg "usb1Rep/key1.crypt.gpg"
	gpg "usb2Rep/key2.crypt.gpg"
	gpg "usb1/key1.crypt.gpg"

	if openssl enc -d -pbkdf2 -aes-256-cbc -in usb1Rep/key1.crypt -out RAMDISK/key1 && openssl enc -d -pbkdf2 -aes-256-cbc -in usb2Rep/key2.crypt -out RAMDISK/key2  && openssl enc -d -pbkdf2 -aes-256-cbc -in usb1/key1.crypt -out RAMDISK/key1  	
	then 
		#we need to remove the Rep word from the keys
		sed "/Rep/d" RAMDISK/key2 > RAMDISK/tempK2
		rm RAMDISK/key2
		mv RAMDISK/tempK2 RAMDISK/key2

		#Decrypting database
		openssl enc -d -pbkdf2 -aes256 -in disque/database.crypt -out disque/databasetmp.crypt -kfile RAMDISK/key2
		openssl enc -d -pbkdf2 -aes256 -in disque/databasetmp.crypt  -out disque/database -kfile RAMDISK/key1
		
		rm disque/database.crypt disque/databasetmp.crypt

		# Creating the new Key for Res2 and Rep2
		dd if=/dev/random bs=64 count=1 > RAMDISK/key2
		cp RAMDISK/key2  RAMDISK/key2Rep #copying the Resp Key to create the Rep key
		echo 'Rep' >> RAMDISK/key2Rep #adding the key word to the Rep Key 

		echo " Key for the new Responsable 2"
		openssl enc -pbkdf2 -aes256 -in RAMDISK/key2 -out usb2/key2.crypt
		gpg -c usb2/key2.crypt

		echo " key for the new Representant 2"
		openssl enc -pbkdf2 -aes256 -in RAMDISK/key2Rep -out usb2Rep/key2.crypt
		gpg -c usb2Rep/key2.crypt

		#Crypting database with the new keys
		openssl enc -pbkdf2 -aes256 -in disque/database -out disque/databasetmp.crypt -kfile RAMDISK/key1
		openssl enc -pbkdf2 -aes256 -in disque/databasetmp.crypt -out disque/database.crypt -kfile RAMDISK/key2

		#Deleting temporary files
		rm RAMDISK/key1 RAMDISK/key2 RAMDISK/key2Rep disque/database disque/databasetmp.crypt

	else 
		echo "Invalid Key ! Please try again ! "		
	fi

elif [ $choice = "REP1" ] #if we choose to repudiate Representant 1
then
	#Then first we have decrypt all the other keys (Res1 && Rep2 && Res2)
	gpg "usb1/key1.crypt.gpg"
	gpg "usb2Rep/key2.crypt.gpg"
	gpg "usb2/key2.crypt.gpg"

	if openssl enc -d -pbkdf2 -aes-256-cbc -in usb1/key1.crypt -out RAMDISK/key1 && openssl enc -d -pbkdf2 -aes-256-cbc -in usb2Rep/key2.crypt -out RAMDISK/key2  && openssl enc -d -pbkdf2 -aes-256-cbc -in usb2/key2.crypt -out RAMDISK/key2  	
	then 
		#we need to remove the Rep word from the keys
		sed "/Rep/d" RAMDISK/key1 > RAMDISK/tempK1
		rm RAMDISK/key1
		mv RAMDISK/tempK1 RAMDISK/key1

		#Decrypting database
		openssl enc -d -pbkdf2 -aes256 -in disque/database.crypt -out disque/databasetmp.crypt -kfile RAMDISK/key2
		openssl enc -d -pbkdf2 -aes256 -in disque/databasetmp.crypt  -out disque/database -kfile RAMDISK/key1
		
		rm disque/database.crypt disque/databasetmp.crypt

		# Creating the new Key for Res1 and Rep1
		dd if=/dev/random bs=64 count=1 > RAMDISK/key1
		cp RAMDISK/key1  RAMDISK/key1Rep #copying the Resp Key to create the Rep key
		echo 'Rep' >> RAMDISK/key1Rep #adding the key word to the Rep Key 

		echo " Key for the new Responsable 1"
		openssl enc -pbkdf2 -aes-256-cbc -in RAMDISK/key1 -out usb1/key1.crypt
		gpg -c usb1/key1.crypt

		echo " key for the new Representant 1"
		openssl enc -pbkdf2 -aes-256-cbc -in RAMDISK/key1Rep -out usb1Rep/key1.crypt
		gpg -c usb1Rep/key1.crypt

		#Crypting database with the new keys
		openssl enc -pbkdf2 -aes-256-cbc -in disque/database -out disque/databasetmp.crypt -kfile RAMDISK/key1
		openssl enc -pbkdf2 -aes-256-cbc  -in disque/databasetmp.crypt -out disque/database.crypt -kfile RAMDISK/key2

		#Deleting temporary files
		rm RAMDISK/key1 RAMDISK/key2 RAMDISK/key1Rep disque/database disque/databasetmp.crypt

	else 
		echo "Invalid Key ! Please try again ! "		
	fi


elif [ $choice = "REP2" ] #if we choose to repudiate Representant 2
then
	#Then first we have decrypt all the other keys (Rep1 && Res1 && Res2)
	gpg "usb1Rep/key1.crypt.gpg"
	gpg "usb2/key2.crypt.gpg"
	gpg "usb1/key1.crypt.gpg"

	if openssl enc -d -pbkdf2 -aes-256-cbc -in usb1Rep/key1.crypt -out RAMDISK/key1 && openssl enc -d -pbkdf2 -aes-256-cbc -in usb1/key1.crypt -out RAMDISK/key1  && openssl enc -d -pbkdf2 -aes-256-cbc -in usb2/key2.crypt -out RAMDISK/key2  	
	then 
		#we need to remove the Rep word from the keys
		sed "/Rep/d" RAMDISK/key2 > RAMDISK/tempK2
		rm RAMDISK/key2
		mv RAMDISK/tempK2 RAMDISK/key2

		#Decrypting database
		openssl enc -d -pbkdf2 -aes256 -in disque/database.crypt -out disque/databasetmp.crypt -kfile RAMDISK/key2
		openssl enc -d -pbkdf2 -aes256 -in disque/databasetmp.crypt  -out disque/database -kfile RAMDISK/key1
		
		rm disque/database.crypt disque/databasetmp.crypt

		# Creating the new Key for Res2 and Rep2
		dd if=/dev/random bs=64 count=1 > RAMDISK/key2
		cp RAMDISK/key2  RAMDISK/key2Rep #copying the Resp Key to create the Rep key
		echo 'Rep' >> RAMDISK/key2Rep #adding the key word to the Rep Key 

		echo " Key for the new Responsable 2"
		openssl enc -pbkdf2 -aes256 -in RAMDISK/key2 -out usb2/key2.crypt
		gpg -c usb2/key2.crypt

		echo " key for the new Representant 2"
		openssl enc -pbkdf2 -aes256 -in RAMDISK/key2Rep -out usb2Rep/key2.crypt
		gpg -c usb2Rep/key2.crypt

		#Crypting database with the new keys
		openssl enc -pbkdf2 -aes256 -in disque/database -out disque/databasetmp.crypt -kfile RAMDISK/key1
		openssl enc -pbkdf2 -aes256 -in disque/databasetmp.crypt -out disque/database.crypt -kfile RAMDISK/key2

		#Deleting temporary files
		rm RAMDISK/key1 RAMDISK/key2 RAMDISK/key2Rep disque/database disque/databasetmp.crypt

	else 
		echo "Invalid Key ! Please try again ! "		
	fi

else
	echo "The choice you made is invalid or doesn't exist please try again !"

fi
