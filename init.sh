#!/bin/bash

#Creating the necessary files and directories
rm -rf usb1 usb2 RAMDISK disque #delete directories if already exist to start the init 
mkdir usb1 usb2 RAMDISK disque
touch disque/database

#Creating key files
dd if=/dev/random bs=64 count=1 | hexdump -ve '1/1 "%02x"' > RAMDISK/key1 #hexdump -ve is used to diplay all input data in a string format
dd if=/dev/random bs=64 count=1 | hexdump -ve '1/1 "%02x"' > RAMDISK/key2

#Creating encrypted files
echo "Responsable 1 key"
openssl enc -pbkdf2 -aes-256-cbc -in RAMDISK/key1 -out usb1/key1.crypt



echo "Responsable 2 key"
openssl enc -pbkdf2 -aes-256-cbc -in RAMDISK/key2 -out usb2/key2.crypt



#Crypting Database
openssl enc -pbkdf2 -aes-256-cbc -in disque/database -out disque/databasetmp.crypt -kfile RAMDISK/key1
openssl enc -pbkdf2 -aes-256-cbc -in disque/databasetmp.crypt  -out disque/database.crypt -kfile RAMDISK/key2

#Deleting temporary files
rm RAMDISK/key1 RAMDISK/key2 disque/database disque/databasetmp.crypt
