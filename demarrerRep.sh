#!/bin/bash

FILE=disque/database
KEYWORD = "Rep"
if [ -f "$FILE" ]; #Check if file exists
then
	echo "Commissioning"
else
    #Decrypting the first key
    read -p "Are you a Responsable 1 (usb1) or a Representant 1 (usb1Rep) ?  " choice1 #prompt option (-p) to avoid adding a trailing newline before trying to read input.
	FIRSTKEYFILE=""$choice1"/key1.crypt"

	if [ -f "$FIRSTKEYFILE" ]; 
	then
		echo "Decrypting the First key"
		openssl enc -d -pbkdf2 -aes-256-cbc -in ""$choice1"/key1.crypt" -out RAMDISK/key1
		if grep "Rep" RAMDISK/key1
		then
			#get the key without the Rep word
			sed "/Rep/d" RAMDISK/key1 > RAMDISK/tempK1
			rm RAMDISK/key1
			mv RAMDISK/tempK1  RAMDISK/key1
		fi
	else
		echo "First Key file is missing in  "$choice1""	
	fi

	#Decrypting the second key
    read -p "Are you a Responsable 2 (usb2) or a Representant 2 (usb2Rep) ?  " choice2
	SECONDKEYFILE=""$choice2"/key2.crypt"

	if [ -f "$SECONDKEYFILE" ]; 
	then
		echo "Decrypting the second key"
		openssl enc -d -pbkdf2 -aes-256-cbc -in ""$choice2"/key2.crypt" -out RAMDISK/key2
		if grep "Rep" RAMDISK/key2
		then
			#get the key without the Rep word
			sed "/Rep/d" RAMDISK/key2 > RAMDISK/tempK2 
			rm RAMDISK/key2
			mv RAMDISK/tempK2  RAMDISK/key2
		fi
		
		#Decrypt database
		openssl enc -d -pbkdf2 -aes-256-cbc -in disque/database.crypt -out disque/databasetmp.crypt -kfile RAMDISK/key2
		openssl enc -d -pbkdf2 -aes-256-cbc -in disque/databasetmp.crypt -out disque/database -kfile RAMDISK/key1

		#Delete crypted files from disque		
		rm disque/databasetmp.crypt disque/database.crypt

	else
		echo "Second Key file is missing in  "$choice2""	
	fi
fi
