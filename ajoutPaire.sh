#!/bin/bash

if [ $# -eq 2 ]
then
	FILE=disque/database
	if [ -f "$FILE" ]; #check if file exists
	then
		echo $1":"$2 >> disque/database #add the pairs to the file
		echo "pair ("$1","$2") added successfully" #confirmation message

	else
		echo "Error adding the pair! Interrupted"
	fi
else
	echo "Error ! Please provide 2 arguments"
fi
